��          �      l      �     �     �               $     6     K     ]     o     �     �     �     �     �          "     7     C     W     f  �  t  *  )     T     Z  <   a  y   �       k  &     �  &   �     �     �     �     	  �   6	  Z   �	     
     4
     :
     F
     M
                   	                         
                                                      sonos_add_lastfmapikey sonos_album sonos_artist sonos_choose_rooms sonos_description sonos_parser_add_row sonos_parser_help sonos_parser_hide sonos_parser_show sonos_placeholder_lastfmapikey sonos_placeholder_slideSpeed sonos_placeholder_updateSpeed sonos_radio_parser_define sonos_set_slide_speed sonos_set_update_speed sonos_thumbnailStyle sonos_title sonos_tn_blackwhite sonos_tn_color sonos_up_next Project-Id-Version: Sonos module for mirr.OS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-14 11:54+0100
PO-Revision-Date: 2018-03-16 16:27+0100
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 To be able to display cover art for some radio stations you must create a LastFM API key. Note: not all radio stations / streams send the artist and title nicely separated, hence it will not be possible to show cover art in those cases unless you tweak the Advanced Radio Parser further down below  Album Artist Choose one or more rooms that you want the mirror to display This module displays the currently playing track and its progress as well as the next title of your selected Sonos rooms. Add a station If you don’t know regex, here is your chance to learn and apply it. The „Station“ must be a regex that identifies the station name you are after. The „field“ is the one field that contains the artist and title mushed together in some parse-able format. The artist and title regexes each must contain a group (those round brackets) that identifies each.  Hide Radio Parser Config Show Advanced Radio Info Parser Config LastFM API key Duration per slide in seconds Seconds between refreshes Advanced Radio Parser Config If you’ve selected more than one room the set will be displayed within a slide show. Set the duration in seconds per slide below You can tune the speed of how often the room/song details get updated. Default value is 3. Choose the style for the slides Sonos Black/White Colour Up next 