<?php
include('../../../config/glancrConfig.php');
include('../vendor/autoload.php');


//load preferred rooms, if they exists
$preferredSonosRoomsExists = FALSE;


$preferredSonosRoomsExists = FALSE;
$preferredSonosRooms= json_decode(getConfigValue('sonos_preferred_rooms'));
if(!empty($preferredSonosRooms)) {
    $preferredSonosRoomsExists = TRUE;
}


use duncan3dc\Sonos\Network;

# Use a custom cache instance that can be cleared on demand
$cache = new \Doctrine\Common\Cache\FilesystemCache('/tmp/sonos-cache');

// $cache->deleteAll();	// TODO: Only on startup/demand

//delete the cache every 10 minutes (if this script gets called at all...)
if( (date('i')%2)==0)
{
	//error_log('Bathroomschedule: Clearing Sonos Controller Cache');
	$cache->deleteAll();
}

$logger = new \Monolog\Logger('sonos');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG));

$sonos = new Network($cache, $logger);
//$sonos = new Network(null, $logger);

function getRemaining($duration, $position) 
{
	$remainingHours = intval(substr($duration, 0, 1)) - intval(substr($position, 0, 1));
	$remainingMinutes = intval(substr($duration, 2, 2)) - intval(substr($position, 2, 2));
	$remainingSeconds = intval(substr($duration, 5, 2)) - intval(substr($position, 5, 2));

	while ($remainingSeconds < 0) {
		$remainingMinutes--;
		$remainingSeconds += 60;
	}

	while ($remainingMinutes < 0) {
		$remainingHours--;
		$remainingMinutes += 60;
	}

	if ($remainingHours < 0) {
		return '0';
		// return '-' . getRemaining($position, $duration); // How long has it already been over
	}

	if ($remainingHours > 0) {
		return array(
			'hours'   => $remainingHours,
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else if ($remainingMinutes > 0) {
		return array(
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else {
		return array(
			'seconds' => $remainingSeconds);
	}
}


function getProgressPercentage($duration, $position)
{
	
	$durationSeconds  = (intval(substr($duration, 0, 1))*3600) + (intval(substr($duration, 2, 2))*60) + intval(substr($duration, 5, 2));
	$progressSeconds = (intval(substr($position, 0, 1)) *3600) + (intval(substr($position, 2, 2))*60) + intval(substr($position, 5, 2)) ;
	
		
	return $durationSeconds==0 ? 0 : round(100*$progressSeconds/ $durationSeconds);

}

try 
{
	// $start = microtime();
	$controllers = $sonos->getControllers();
	// $end = microtime();
    $foundPreferredRoom=false;
	$allRooms = array();
	$allRooms['rooms'] = array();
	
	$roomSpeakers= array();
	
	//error_log("-------------------" . print_r($preferredSonosRooms,true) . "--------------");
	
	if ($controllers != null && $preferredSonosRoomsExists) 
	{
		foreach($controllers as $controller) 
		{
			$data = array();
		
			
	        foreach($preferredSonosRooms as $preRoom)
			{
				//error_log("getSonosControllerState:  " . print_r($controller,true));
				//print_r($controller,true);
				$roomSpeakers=$controller->getSpeakers();
				$data['room']='';
		        foreach($roomSpeakers as $speaker) 
				{
					//error_log("getSonosControllerState: roomSpeaker {$speaker->room}");
					//if(strlen($data['room'] . $speaker->room)<24)
					//{
						if(trim($data['room'])==false)
							$data['room']=$speaker->room;
						else
							$data['room']=$data['room'] . '+' . $speaker->room;
						
							
						//}
					//else
					//{
					//	$data['room']=substr($data['room'] . ' + ' . $speaker->room,0,21) . '...';
					//}
					
				
					
					//error_log("getSonosControllerState: check if they are the same:" . strcmp($speaker->room,$preRoom));
					if(strcmp($speaker->room,$preRoom)==0)
			        {
						//error_log("getSonosControllerState: found it!!!!");  
						$foundPreferredRoom=true;
					
			        }
					
				}
				
				
				
			    if ($foundPreferredRoom)
			    {   
					$temproomname = explode('+', $data['room']);
					sort($temproomname, SORT_STRING);
					//error_log(print_r($temproomname,TRUE));
					$temproomname=implode(' + ',$temproomname);
					//error_log(print_r($temproomname,TRUE));
			
			
					$data['room']=trim($temproomname);
					//error_log("new room name " . $data['room']);
			
					if(strlen($data['room']) >26)
						$data['room']=substr($data['room'],0,23) . ' ...';
				
					//$data['room']='sorted' . $data['room'];
					//error_log("new room name final " . $data['room']);
				    
					break;
			    }
				
			}
		    if (!$foundPreferredRoom)
		    {       
				continue;
		    }
			else
			{
				//error_log("getSonosControllerState: found room");
				$foundPreferredRoom=false;
			}
			
			

			

			$simpleState = $controller->getState();

			if ($simpleState == $controller::STATE_PLAYING) 
			{
				if (($state = $controller->getStateDetails()) != null && ($state->stream != null || $state->title != '')) 
				{
					////error_log(print_r($state,true));
					if ($state->stream != null) 
					{
						$data['state'] = 'streaming';
						//error_log("stream info: ". $state->stream);
						if(strcmp($state->stream, 'Line-In')==0)
						{
							$data['thumbnail'] = '/modules/sonos/assets/linein.svg';
						}
						else
						{
							$data['thumbnail'] = '/modules/sonos/assets/streaming.svg';
						}
						$data['current'] =  $state->stream;
						$data['title'] = $state->title;
						$data['artist'] = $state->artist;	
					} 
					
					else 
					{
						$data['state'] = 'playing';
						$data['thumbnail'] = $state->albumArt;
						//$data['current'] = $state->title . ' • ' . $state->artist;
						$data['remaining'] = getRemaining($state->duration, $state->position);
						$data['progressPct'] =  getProgressPercentage($state->duration, $state->position);
						$data['title'] = $state->title;
						$data['artist'] = $state->artist;
						$data['album'] = $state->album;		
						
					}

					if ($controller->isUsingQueue()) 
					{
						$queue = $controller->getQueue();

						// Next 4 tracks (if avaliable)
						if (count($nextTracksArray = $queue->getTracks($state->queueNumber + 1, 1)) > 0) 
						{
							$data['next'] = '';
							for ($i = 0; count($nextTracksArray) > $i; $i++) 
							{
								$data['next'] .= $nextTracksArray[$i]->title . ' • ' . $nextTracksArray[$i]->artist . (count($nextTracksArray) > $i + 1 ? '' : '');
							}
						}
					}
				}
			} 
			else 
			{
				$data['thumbnail'] = '/modules/sonos/assets/nosound.svg';
				$data['state'] = 'shushed';
			}

			array_push($allRooms['rooms'],$data);	
			
		} //end looping through all controllers/rooms
		
		// $data['time'] = 1000 * ($end - $start) . ' msec';

		function cmp($a, $b)
		{
		    return strcmp($a['room'], $b['room']);
		}

		usort($allRooms['rooms'], "cmp");
		

		if (isset($_GET['decode'])) 
		{	// Debug-reasons only
			print_r($allRooms);
		} 
		else 
		{
			echo json_encode($allRooms);
		} 
	}
} 
catch (RuntimeException $e) 
{
	// empty response - no sonos found
	echo json_encode('');
}

?>