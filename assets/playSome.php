<?php
include('../../../config/glancrConfig.php');
include('../vendor/autoload.php');


//load preferred rooms, if they exists
$preferredSonosRoomsExists = FALSE;


$preferredSonosRooms= json_decode(getConfigValue('preferred_sonos_rooms'));
if(!empty($preferredSonosRooms)) {
    $preferredSonosRoomsExists = TRUE;
}


use duncan3dc\Sonos\Network;

# Use a custom cache instance that can be cleared on demand
$cache = new \Doctrine\Common\Cache\FilesystemCache('/tmp/sonos-cache');
// $cache->deleteAll();	// TODO: Only on startup/demand
$logger = new \Monolog\Logger('sonos');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG));

$sonos = new Network($cache, $logger);

function getRemaining($duration, $position) {
	$remainingHours = intval(substr($duration, 0, 1)) - intval(substr($position, 0, 1));
	$remainingMinutes = intval(substr($duration, 2, 2)) - intval(substr($position, 2, 2));
	$remainingSeconds = intval(substr($duration, 5, 2)) - intval(substr($position, 5, 2));

	while ($remainingSeconds < 0) {
		$remainingMinutes--;
		$remainingSeconds += 60;
	}

	while ($remainingMinutes < 0) {
		$remainingHours--;
		$remainingMinutes += 60;
	}

	if ($remainingHours < 0) {
		return '0';
		// return '-' . getRemaining($position, $duration); // How long has it already been over
	}

	if ($remainingHours > 0) {
		return array(
			'hours'   => $remainingHours,
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else if ($remainingMinutes > 0) {
		return array(
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else {
		return array(
			'seconds' => $remainingSeconds);
	}
}

try 
{
	// $start = microtime();
	$controllers = $sonos->getControllers();
	// $end = microtime();
    $foundPreferredRoom=false;
	$allRooms = array();
	$allRooms['rooms'] = array();
	if ($controllers != null && $preferredSonosRoomsExists) 
	{
		foreach($controllers as $controller) 
		{
	        
			if(strcmp($controller->room,'Bad Oben')==0)
			{
				$controller->setVolume(1);
			    if ($controller->getState() !== $controller::STATE_PLAYING) 
				{
			           $controller->play();
				}
			}

			
		} //end looping through all controllers/rooms
		
		
	}
} 
catch (RuntimeException $e) 
{
	// empty response - no sonos found
	echo json_encode('');
}

?>