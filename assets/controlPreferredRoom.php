<?php
include('../../../config/glancrConfig.php');
include('../vendor/autoload.php');


//load preferred rooms, if they exists
$controlledRoomExists = FALSE;
$controlledRoom= json_decode(getConfigValue('sonos_controlled_room'));
if(!empty($controlledRoom)) {
    $controlledRoomExists = TRUE;
}
else
{
	$controlledRoom="Bad Oben";
	
	$controlledRoom="K\u00fcche";
}


use duncan3dc\Sonos\Network;
use duncan3dc\Sonos\Controller;
use duncan3dc\Sonos\State;
# Use a custom cache instance that can be cleared on demand
$cache = new \Doctrine\Common\Cache\FilesystemCache('/tmp/sonos-cache');
// $cache->deleteAll();	// TODO: Only on startup/demand
$logger = new \Monolog\Logger('sonos');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG));

$sonos = new Network($cache, $logger);


// now first make things generic so this script works from command line as well as via the web
foreach ($argv as $arg) 
{
    $e=explode("=",$arg);
    if(count($e)==2)
        $_GET[$e[0]]=$e[1];
    else    
        $_GET[$e[0]]=0;
}

try 
{
	$controllerRoom = $sonos->getControllerByRoom($controlledRoom);
	$speakerRoom = $sonos->getSpeakersByRoom($controlledRoom);
	
	//now check what the caller wanted.
	$crState=$controllerRoom->getStateDetails();
	
	switch($_GET["action"])
	{
		case "next":	// first, if it is paused then unmute and play and then next title (if applicable)
						error_log("controlPreferredRoom: action=next");
						if ($controllerRoom->isMuted() || $controllerRoom->getStateName() != 'PLAYING') 
						{
						    $controllerRoom->unmute();
							$controllerRoom->setVolume(15); //make sure we don't blow our hearing to hell...
							$controllerRoom->play();
						}
						elseif (is_null($crState->stream)) 
						{
   						 	$controllerRoom->next();
						}
					    
						break;
		case "previous":// if on radio stream, stop the stream, otherwise previous track
						error_log("controlPreferredRoom: action=previous");
					
						
						
						if (!is_null($crState->stream) && $controllerRoom->getStateName() == 'PLAYING') 
						{
								error_log("controlPreferredRoom: pausing");
						        $controllerRoom->pause(); 
						}
						else if ($crState->queueNumber >0 && $controllerRoom->getStateName() == 'PLAYING') 
						{
							error_log("controlPreferredRoom: previous");
						 	$controllerRoom->previous();
						}
						break;
		case "louder":  //volume ++ but never above 60 (kids....), if current volume is 0, then increase and  start to play.		
						error_log("controlPreferredRoom: action=louder");
						if ($controllerRoom->getState() == Controller::STATE_PLAYING && $controllerRoom->getVolume()<3) 
						{
							$controllerRoom->unmute();
						}
						if ($controllerRoom->getState() == Controller::STATE_PLAYING && $controllerRoom->getVolume()<60) 
						{
							$controllerRoom->adjustVolume(5);
						}
						break;
		case "quieter": //volume --, if new value is 0, then stop via mute.
						error_log("controlPreferredRoom: action=quieter");
						if ($controllerRoom->getState() == Controller::STATE_PLAYING && $controllerRoom->getVolume()>0) 
						{
							$controllerRoom->adjustVolume(-5);
							if ($controllerRoom->getVolume()<5)
							{
								$controllerRoom->setVolume(0);
								$controllerRoom->mute();
							}
						}
						break;
		case "stop": //volume --, if new value is 0, then stop via mute.
						error_log("controlPreferredRoom: action=stop");
						if ($controllerRoom->getState() == Controller::STATE_PLAYING && !$controllerRoom->isMuted()) 
						{
						    $controllerRoom->mute();
							$controllerRoom->pause();
						}
						break;
		
	}
	
	
	
} 
catch (RuntimeException $e) 
{
	// empty response - no sonos found
	echo json_encode('');
}

?>