<?php
// include('../../../config/glancrConfig.php');
include('../vendor/autoload.php');




use duncan3dc\Sonos\Network;

# Use a custom cache instance that can be cleared on demand
$cache = new \Doctrine\Common\Cache\FilesystemCache('/tmp/sonos-cache');

//delete the cache every 10 minutes (if this script gets called at all...)
if( (date('i')%10)==0)
{
	//error_log('Bathroomschedule: Clearing Sonos Controller Cache');
	$cache->deleteAll();
}



$logger = new \Monolog\Logger('sonos');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG));

$sonos = new Network($cache, $logger);

function getRemaining($duration, $position) {
	$remainingHours = intval(substr($duration, 0, 1)) - intval(substr($position, 0, 1));
	$remainingMinutes = intval(substr($duration, 2, 2)) - intval(substr($position, 2, 2));
	$remainingSeconds = intval(substr($duration, 5, 2)) - intval(substr($position, 5, 2));

	while ($remainingSeconds < 0) {
		$remainingMinutes--;
		$remainingSeconds += 60;
	}

	while ($remainingMinutes < 0) {
		$remainingHours--;
		$remainingMinutes += 60;
	}

	if ($remainingHours < 0) {
		return '0';
		// return '-' . getRemaining($position, $duration); // How long has it already been over?
	}

	if ($remainingHours > 0) {
		return array(
			'hours'   => $remainingHours,
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else if ($remainingMinutes > 0) {
		return array(
			'minutes' => $remainingMinutes,
			'seconds' => $remainingSeconds);
	} else {
		return array(
			'seconds' => $remainingSeconds);
	}
}

try 
{
    // $start = microtime();
    $controllers = $sonos->getControllers();
    // $end = microtime();
	
	//error_log(print_r($controllers,true));
	
	$allRooms = array();
	$allRooms['rooms'] = array();
	$roomSpeakers= array();
	
    if ($controllers != null) 
	{
        foreach($controllers as $controller) 
		{
			$roomSpeakers=$controller->getSpeakers();
		    if(count($roomSpeakers)>0)
			{
		        foreach($roomSpeakers as $speaker) 
				{
			
	            	$data = array();

	            	$data['room'] = $speaker->room;
				
				
					array_push($allRooms['rooms'],$data);
				}
			}
			else
			{
            	$data = array();

            	$data['room'] = $controller->room;
			
			
				array_push($allRooms['rooms'],$data);
			}
			

        

       

        } //end looping through all controllers/rooms

        // $data['time'] = 1000 * ($end - $start) . ' msec';

        if (isset($_GET['decode'])) {    // Debug-reasons only
            print_r($allRooms);
        } else {
            echo json_encode($allRooms);
        }
    }
}
catch (RuntimeException $e) 
{
	// empty response - no sonos found
	echo json_encode('');
}

?>