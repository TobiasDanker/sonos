var hoursRemaining = 0;
var minutesRemaining = 0;
var secondsRemaining = 0;
var timeInterval;
var requestInterval;
var syncMistake = 0;	// (+) -> voraus; (-) -> hinterher


var slides;
var preferredRoomsLength=0;
var rebuildRoomList=true;
var jsonRoomsControl={ "rooms": []};
var jsonRoomsCurrent={ "rooms": []};

	  
var currentSlide = 0;
var currentDot=0;

var slides ;
var dots;

var slideInterval;

var progressbar;

var lockUpdateDetails = 0;
var lockUpdateRooms = 0;




function changeThumbnailSmoothly(src) {
	var $sonosThumbnail = $('.'+thumbnailStyle);

	if ($sonosThumbnail.children().length == 0 || $sonosThumbnail.children().attr("src") != src) {
        if ($sonosThumbnail.children().length == 0) {
            $sonosThumbnail.html("");
            $sonosThumbnail.append("<img>");
			// TODO: Add smoothness
        }
		$sonosThumbnail.children().attr("src", src);
	}
}

function updateRoomsThumbnail()
 {
  
  var lastfmUrl;
  var actualThumbnail;
  var k=0;
  foundIndex=-1;
  roomList=jsonRoomsCurrent;
  //console.log("updateRoomsThumbnail");
  //console.log(jsonRoomsControl);
  if(roomList == undefined)
	  return;
	for (k=0;k<roomList["rooms"].length;k++)
	{	
		
		
		
		//if(k!=currentSlide) continue;	
		//console.log("updating slide="+k+" dotslide="+currentSlide);
		
		
		actualThumbnail=roomList["rooms"][k]["thumbnail"];

		if(  lastfmapikey !== undefined && lastfmapikey !='' && roomList["rooms"][k]["artist"] !== undefined &&  roomList["rooms"][k]["title"]  !== undefined && roomList["rooms"][k]["artist"].length>1  &&  roomList["rooms"][k]["title"].length>1 && roomList["rooms"][k]["state"]== 'streaming')
		{ 
			lastfmUrl='http://ws.audioscrobbler.com/2.0/?method=track.search&track='+ encodeURI(roomList["rooms"][k]["title"])+'&artist='+encodeURI(roomList["rooms"][k]["artist"])+'&api_key='+lastfmapikey+'&format=json&limit=1';
			//console.log("lastFM url OUSIDE ajax="+lastfmUrl);
			foundIndex=k;
				$.ajax({
			  	  			cache: true,
			 	   			url: lastfmUrl,
			  	  			dataType: "json",
			  	  		}).done(
								function(imageSearchResults) 
								{
			
									//console.log("lastFM url in AJAX="+lastfmUrl);
									//alert("result lastfm" + imageSearchResults.results.trackmatches.track[0].image[3]["#text"]);
									//console.log(imageSearchResults.results.trackmatches.track[0]);
						 			if( imageSearchResults.results.trackmatches.track[0]!== undefined)
									{ 
										//console.log("sonos set album art: found image="+imageSearchResults.results.trackmatches.track[0].image[3]["#text"]);
										//console.log("sonos set album art: current image="+$("#sonosThumbnail"+foundIndex).children().attr("src"));
										if(imageSearchResults.results.trackmatches.track[0].image[3]["#text"].length > 5 && $("#sonosThumbnail"+foundIndex).children().attr("src")!=imageSearchResults.results.trackmatches.track[0].image[3]["#text"])
										{
											//damn jquery call to the DOM not working. using pure javascript...
											//alert("<img class=\"sonosThumbnail\" src=\""+imageSearchResults.results.trackmatches.track[0].image[3]["#text"]+"\">");
											//alert('found at: '+foundIndex);
											//console.log("setting new image from lastFM!!");
											if (document.getElementById("sonosThumbnail"+foundIndex).innerHTML !== null)
											{
												
											
											document.getElementById("sonosThumbnail"+foundIndex).innerHTML="<img class=\""+thumbnailStyle+"\" src=\""+imageSearchResults.results.trackmatches.track[0].image[3]["#text"]+"\">";
											}
											actualThumbnail=imageSearchResults.results.trackmatches.track[0].image[3]["#text"];
										}
					
									}
									else
									{
										//console.log($("#sonosThumbnail"+foundIndex).children().attr("src"));
										//console.log("setting new image from default!!");
										if($("#sonosThumbnail"+foundIndex).children().attr("src")!=actualThumbnail)
										{
											$("#sonosThumbnail"+foundIndex).html("<img class=\""+thumbnailStyle+"\" src=\""+actualThumbnail+"\">");
											
										}
									}
								

								}
							  );
						
		     	
			
	
		
		}
		else
		{
			//console.log($("#sonosThumbnail"+k).children().attr("src"));
			//console.log("setting new image from default-default!");
			if($("#sonosThumbnail"+k).children().children().attr("src")!=roomList["rooms"][k]["thumbnail"])
			{
				$("#sonosThumbnail"+k).html("<img class=\""+thumbnailStyle+"\" src=\""+roomList["rooms"][k]["thumbnail"]+"\">");
			}
		}
	
	}
 }

function updateRoomsDetails()
{
	//console.log("updateRoomsDetails");
	//console.log(jsonRoomsCurrent);
	
	if(lockUpdateDetails==1 || lockUpdateRooms==1) 
		{ 
			//console.log("locked update"); 
			return; 
		}
		
	console.log("setting lockUpdateDetails to 1");
	lockUpdateDetails=1;
	
		$.ajax({
 	  			cache: false,
	   			url: "/modules/sonos/assets/getSonosControllersState.php",
 	  			dataType: "json",
 	  	}).done(
 		   	function(jsonUpdateRooms) 			
	   		{
				//console.log("sonos: fetched room states:");
				//console.log(jsonUpdateRooms);
				jsonRoomsCurrent=jsonUpdateRooms;
				
				if (jsonRoomsCurrent.rooms == undefined)
					return;
				
				// TODO: Showing info must be avoided using the show() jquery. it must be more a delete/attach so the displaying doesnt get confused for multiple rooms.
				
			 	for (sonos_i=0;sonos_i<jsonRoomsCurrent["rooms"].length;sonos_i++)
				{	
					
					if(sonos_i!=currentSlide) continue;
					//console.log("updating details sonos_i="+sonos_i+" currentSlide="+currentSlide);		
					
					
					
					$("#sonosRoom"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["room"]);
					//$("#sonosCurrent"+i).html(jsonRoomsCurrent["rooms"][i]["current"]== undefined ? "" : jsonRoomsCurrent["rooms"][i]["current"]);
		    
				    //console.log("current="+jsonRoomsCurrent["rooms"][sonos_i]["current"]);
					//console.log("state="+jsonRoomsCurrent["rooms"][sonos_i]["state"]);
					if(jsonRoomsCurrent["rooms"][sonos_i]["current"]!=null && jsonRoomsCurrent["rooms"][sonos_i]["state"]=='streaming')
					{
						//console.log("sonos: updating room when radio is playing...");
						
						$("#sonosStation"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["current"]);
						$("#sonosStation"+sonos_i).parent().parent().show();
						
						
						for(ri=0;ri<radioInfoParser.length;ri++)
						{
							checkStationRE=RegExp(radioInfoParser[ri]['station'],'g');
						
							if(checkStationRE.exec(jsonRoomsCurrent["rooms"][sonos_i]["current"]) !== null)
							{
								//console.log("sonos: we have a regex hit on this station!");
							
								//console.log("artist regex="+radioInfoParser[ri]['artist']);
								//console.log("title regex="+radioInfoParser[ri]['title']);
								getArtistRE=RegExp(radioInfoParser[ri]['artist'],'g');
								getTitleRE=RegExp(radioInfoParser[ri]['title'],'g');
							
								//console.log("field to search in ="+radioInfoParser[ri]['field']);
								//console.log("fieldvalue to search ="+jsonRoomsCurrent["rooms"][sonos_i][radioInfoParser[ri]['field']]);
							
								artistStrings=getArtistRE.exec(jsonRoomsCurrent["rooms"][sonos_i][radioInfoParser[ri]['field']]);
								titleStrings=getTitleRE.exec(jsonRoomsCurrent["rooms"][sonos_i][radioInfoParser[ri]['field']]);
								if(artistStrings != null)
								{
									jsonRoomsCurrent["rooms"][sonos_i]['artist']=artistStrings[1];
									//console.log("found artist="+artistStrings[1]);
								}
								//else
									//console.log("no artist found.");
								if(titleStrings != null)
								{
									jsonRoomsCurrent["rooms"][sonos_i]['title']=titleStrings[1]
									//console.log("found title="+titleStrings[1]);
								}
								 //else 
									//console.log("no title found.");
							
							}
						}
				
					}
					
					//
					
					else
					{
						$("#sonosStation"+sonos_i).html("");
						$("#sonosStation"+sonos_i).parent().parent().hide();
					}
			
					//alert(jsonRoomsCurrent["rooms"][i]["thumbnail"]);
					if (jsonRoomsControl !== undefined && jsonRoomsCurrent !== undefined && jsonRoomsControl["rooms"][sonos_i] !== undefined && jsonRoomsCurrent["rooms"][sonos_i] !== undefined )
					{
						jsonRoomsControl["rooms"][sonos_i]["thumbnail"]=jsonRoomsCurrent["rooms"][sonos_i]["thumbnail"]== undefined ? "" : jsonRoomsCurrent["rooms"][sonos_i]["thumbnail"];
					}
			
					if(jsonRoomsCurrent["rooms"][sonos_i]["progressPct"]!== undefined && document.getElementById("roomProgressBar"+sonos_i) !== undefined )
					{
						//console.log("roomProgressBar stuff, index="+sonos_i);
						
						if(jsonRoomsCurrent["rooms"][sonos_i]["state"]!=='streaming')
							document.getElementById("roomProgressBar"+sonos_i).style.width= jsonRoomsCurrent["rooms"][sonos_i]["progressPct"] + "%";
							
						
					}
					else
						if(document.getElementById("roomProgressBar"+sonos_i) !== null)
							document.getElementById("roomProgressBar"+sonos_i).style.width= "0%";
			
				
					
					
					if(jsonRoomsCurrent["rooms"][sonos_i]["title"]==null || jsonRoomsCurrent["rooms"][sonos_i]["title"].length<1 )
					{
						$("#sonosTitle"+sonos_i).html("");
						$("#sonosTitle"+sonos_i).parent().parent().hide();
					}
					else
					{
						//console.log("sonos: updating title.");
						$("#sonosTitle"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["title"]);
						$("#sonosTitle"+sonos_i).parent().parent().show();
				
					}
		
		
		
					if(jsonRoomsCurrent["rooms"][sonos_i]["artist"]== undefined || jsonRoomsCurrent["rooms"][sonos_i]["current"]=="Line-In")
					{
						$("#sonosArtist"+sonos_i).html("");
						$("#sonosArtist"+sonos_i).parent().parent().hide();
					}
					else
					{
						$("#sonosArtist"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["artist"]);
						
						$("#sonosArtist"+sonos_i).parent().parent().show();
				
					}
			
					if(jsonRoomsCurrent["rooms"][sonos_i]["album"]== undefined )
					{
						$("#sonosAlbum"+sonos_i).html("");
						$("#sonosAlbum"+sonos_i).parent().parent().hide();
					}
					else
					{
						$("#sonosAlbum"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["album"]);
						$("#sonosAlbum"+sonos_i).parent().parent().show();
				
					}
			
					if(jsonRoomsCurrent["rooms"][sonos_i]["next"]== undefined )
					{
						$("#sonosNext"+sonos_i).html("");
						$("#sonosNext"+sonos_i).parent().parent().hide();
					}
					else
					{
						$("#sonosNext"+sonos_i).html(jsonRoomsCurrent["rooms"][sonos_i]["next"]);
						$("#sonosNext"+sonos_i).parent().parent().show();
				
					}
	
				}
				
				//adjusted from using jquery to normal DOM traversal
				
				containers = document.querySelectorAll('#sonos_infoTable');
				for (c=0;c<containers.length;c++)
				{
					//Array.prototype.forEach.call(containers, (container) => {  // Loop through each container
						//alert('ding'+container.innerHTML);
					      p = containers[c].querySelector('tr > td > div');
					      divh = containers[c].clientHeight;
					    while (p.offsetHeight > divh) { // Check if the paragraph's height is taller than the container's height. If it is:
					       p.textContent = p.textContent.replace(/\W*\s(\S)*$/, '...'); // add an ellipsis at the last shown space
					    }
				//	});
				}
				
				console.log("resetting lockUpdateDetails to 0");
				lockUpdateDetails=0;
				
			});
//alert("after room details:"+jsonRoomsControl["rooms"][1]["thumbnail"].toString());
}

 function updateRoomsList()
 {
	 //console.log("updateRoomsList");

	 if(lockUpdateDetails==1 || lockUpdateRooms==1) 	 return;
     
	 console.log("setting lockUpdateRooms to 1");
	 lockUpdateRooms=1;

	 $.ajax({
	   			cache: false,
	 	 		url: '/modules/sonos/assets/getSonosControllersState.php',
	 		    dataType: "json"
	 		  }).done(
				 function(jsonRoomsCurrent,status,xhr) 	
				  {
					 
					 removeMissingRoom=false;
					 console.log("sonos: updateroomsList, raw info returned and current set:");
					 console.log(jsonRoomsCurrent);
					 console.log(jsonRoomsControl);
					 for( sonos_i = 0; sonos_i < jsonRoomsControl["rooms"].length; sonos_i++)
			           {
			               removeMissingRoom=true;
			               //alert("checking outer="+jsonRoomsControl["rooms"][i]["room"]);
			                 //console.log("sonos: looping through all rooms to find the one to remove next");
			               for (  sonos_j=0; sonos_j<jsonRoomsCurrent["rooms"].length; sonos_j++)
			               {
			                 //console.log("old="+jsonRoomsControl["rooms"][sonos_i]["room"] + " new="+jsonRoomsCurrent["rooms"][sonos_j]["room"]);
			                 if ( jsonRoomsControl["rooms"][sonos_i]["room"]==jsonRoomsCurrent["rooms"][sonos_j]["room"])
			                 {
			                   //console.log("sonos: match, not removing");
			                   removeMissingRoom=false;

			                 }
			                 else
			                 {
			                   //console.log("sonos: NO match,  TAGGED FOR REMOVING");
			                 }

			               }
			               if (removeMissingRoom)
			               {
							   break;
			               }
			             }
						 
					     //console.log("sonos: checking what rooms to add");
		            for (sonos_i=0; sonos_i<jsonRoomsCurrent["rooms"].length;sonos_i++)
		            {
		              var addNewRoom=true;
		              for (sonos_j=0;sonos_j<jsonRoomsControl["rooms"].length;sonos_j++)
		              {
		                //console.log("old="+jsonRoomsControl["rooms"][sonos_i]["room"] + " new="+jsonRoomsCurrent["rooms"][sonos_j]["room"]);
		                if ( jsonRoomsControl["rooms"][sonos_j]["room"]==jsonRoomsCurrent["rooms"][sonos_i]["room"])
		                {
		                  addNewRoom=false;
		                  break;
		                }
		              }
		              if (addNewRoom)
		              {
						  break;
		              }

		            }
					if (addNewRoom || removeMissingRoom)
					{
						//if (updateIntervalUpdateRoomsThumbnail != undefined) clearInterval(updateIntervalUpdateRoomsThumbnail);
					    //if (slideInterval != undefined) clearInterval(slideInterval);
			 		    //if (updateIntervalUpdateRoomsDetails != undefined) clearInterval(updateIntervalUpdateRoomsDetails);
						//if (updateIntervalUpdateRoomsList != undefined) clearInterval(updateIntervalUpdateRoomsList);
						
					
 			 		    console.log("sonos: adding or removing romes...");
					
	    				//start anew
	  					$("#roomScroller").empty();
	  					$("#dotScroller").empty();

	  					jsonRoomsControl["rooms"]=jsonRoomsCurrent["rooms"];
						//console.log(jsonRoomsControl["rooms"]);
   					 	//console.log("sonos: updateroomsList, processed new room info:");
   					 	//console.log(jsonRoomsControl);
					 

	  					  newDotHTML="";
						  newRoomHTML="";
	  					  for( sonos_i = 0; sonos_i < jsonRoomsControl["rooms"].length; sonos_i++)
	  					  {
	  						  
	  			      			  //console.log("sonos: now adding new room");
		  
	  							  jsonRoomsControl["rooms"][sonos_i]["slideraction"]="keep";
	  						  		 newRoomHTML+='<div id="sonosRoomTile'+sonos_i+'" class="slide">'+
	  						  							'<div class="sonosLeft"> '+
	  							  							'<div class="roomProgress" id="roomProgress'+sonos_i+'">'+
	  							  								'<div class="roomProgressBar" id="roomProgressBar'+sonos_i+'">'+
	  							  								'</div>'+
	  							  							'</div> '+
	  						  								'<div class="'+thumbnailStyle+'" name="sonosThumbnail" id="sonosThumbnail'+sonos_i+'">'+
	  							  							'</div> '+
	  						  								'<div class="thumbnailCenter"> '+
	  						  									'<div class="sonosRoom" id="sonosRoom'+sonos_i+'"></div> '+
	  						  									'<div class="sonosRemaining" id="sonosRemaining'+sonos_i+'"></div>'+
	  						  								'</div> '+
	  						  				  			'</div> '+
	  						  				 	 		'<div class="sonosRight"> '+
	  						  								'<table width="100%" id="sonos_infoTable">'+
	  							  								'<tr>'+
	  									  							'<td width="20%">'+
	  									  								'<div class="sonosInfoTitle">'+textStation+'</div>'+
	  									  							'</td>'+
	  									  							'<td width="80%">'+
	  												  					'<div class="sonosStation" id="sonosStation'+sonos_i+'"></div> '+
	  									  							'</td>'+
	  									  						'</tr>'+
	  									  						'<tr>'+
	  									  							'<td width="20%">'+
	  									  								'<div class="sonosInfoTitle">'+textTitle+'</div>'+
	  									  							'</td>'+
	  									  							'<td width="80%">'+
	  												  					'<div class="sonosTitle" id="sonosTitle'+sonos_i+'"></div> '+
	  									  							'</td>'+
	  									  						'</tr>'+
	  									  						'<tr>'+
	  									  							'<td width="20%">'+
	  									  								'<div class="sonosInfoTitle">'+textAlbum+'</div>'+
	  									  							'</td>'+
	  									  							'<td width="80%">'+
	  												 	 				'<div class="sonosAlbum" id="sonosAlbum'+sonos_i+'"></div> '+
	  									  							'</td>'+
	  									  						'</tr>'+
	  									  						'<tr>'+
	  									  							'<td width="20%">'+
	  									  								'<div class="sonosInfoTitle">'+textArtist+'</div>'+
	  									  							'</td>'+
	  									  							'<td width="80%">'+
	  												  					'<div class="sonosArtist" id="sonosArtist'+sonos_i+'"></div> '+
	  									  							'</td>'+
	  									  						'</tr>'+
	  									  						'<tr>'+
	  									  							'<td width="20%">'+
	  									  								'<div class="sonosInfoTitle">'+textUpNext+'</div>'+
	  									  							'</td>'+
	  									  							'<td width="80%">'+
	  												  					'<div class="sonosNext" id="sonosNext'+sonos_i+'"></div>'+
	  									  							'</td>'+
	  									  						'</tr>'+
	  									  					 '</table>'+
	  						  				   			'</div>' +
	  							  					'</div>';

	  						 				 



	  						 				 newDotHTML+="<div class=\"dot\"></div>";
	  						  			   

	  						
	  					  } // end looping through rooms

						   $("#roomScroller").empty().append(newRoomHTML);
						   $("#dotScroller").empty().append(newDotHTML);  
						  
						  //console.log("sonos: roomScroller html:");
						  //console.log($("#roomScroller").html());

	  				      //make sure our slider has all the info needed

	  					  slides = document.querySelectorAll('#roomScroller .slide');
	  					  dots = document.querySelectorAll('#dotScroller .dot');

	  					  // animate only if we have more than one room...
						  //currentSlide = 0;  	
  					      //slides[0].className = 'slide showing';
  						  //currentDot=0;
  						  //dots[0].className ='dot hidden';
						  
    			 		   //updateIntervalUpdateRoomsThumbnail=setInterval(function() { updateRoomsThumbnail(); },refreshIntervalUpdateRoomsThumbnail);
    			 		   //updateIntervalUpdateRoomsDetails=setInterval(function() { updateRoomsDetails(); },refreshIntervalUpdateRoomsDetails);
    			 		   //updateIntervalUpdateRoomsList=setInterval(function() { updateRoomsList(); },refreshIntervalUpdateRoomsList);
			 		
						  
	  					  if (jsonRoomsCurrent["rooms"].length > 1 && slideInterval == undefined)
	  					  {	
							  console.log("preping scroller for more than one room")	;  
	  					  	slideInterval=setInterval(nextSlide,slideSpeed);
	  					  }
	  					   if (jsonRoomsCurrent["rooms"].length < 2 )
	  					  {
							  console.log("preping scroller for  just one room")	;  
	  						  if (slideInterval != undefined) clearInterval(slideInterval);
	  					      slides[0].className = 'slide showing';
	  						  currentDot=0;
	  						  $("#dotScroller .dot").attr('class', 'dot hidden');
							  //dots[0].className ='dot hidden';
	  					  }
					  
  
					  }
					  
					  //updateRoomsDetails();
	  				  console.log("resetting lockUpdateRooms to 0");
					  lockUpdateRooms=0;
				  });
				  		
	  		  		
	
 }

 